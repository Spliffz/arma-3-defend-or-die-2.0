//  Defend Or Die 2.0
//  init.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>


// is player JIPper? 
if(!isServer && isNull player) then {
	isJIP = true;
} else {
	isJIP = false;
};

if(!isDedicated) then {
	waitUntil {	!isNull player && isPlayer player };
};

if(isNil "isJIP") then {
	isJIP = false;
	player setVariable ["isJIP", false, true];
} else {
	if (isJIP) then {
		player setVariable ["isJIP", true, true];
	} else {
		player setVariable ["isJIP", false, true];
	};
};

// do gameplay-improving stuff
enableSaving [false, false]; // no saving allowed. Defend it or Die!
enableSentences false; // no more annoying sentences from AI


// get and parse mission parameters
#include "lib\functions\parseParameters.sqf"

// include defines
#include "lib\defines.sqf"


// make the functions and scripts ready for use
// General functions
DOD_fnc_setTimeDate = compile preprocessFileLineNumbers "lib\functions\fnc_setDate.sqf";
DOD_pick_defPos = compile preprocessFileLineNumbers "lib\pick_defPos.sqf";
DOD_startUp = compile preprocessFileLineNumbers "lib\startUp.sqf";
DOD_timer = compile preprocessFileLineNumbers "lib\timer.sqf";
DOD_BodyRemovalService = compile preprocessFileLineNumbers "lib\gc\garbageCollector.sqf";
DOD_JIP_init = compile preprocessFileLineNumbers "lib\functions\fnc_setJIP.sqf";
DOD_fnc_n_startNextRound = compile preprocessFileLineNumbers "lib\functions\fnc_n_startNextRound.sqf";
DOD_start_helmetCam = compile preprocessFileLineNumbers "lib\starthcam.sqf";
DOD_fnc_init_VAS = compile preprocessFileLineNumbers "lib\functions\fnc_init_vas.sqf";
DOD_fnc_requestClientID = compile preprocessFileLineNumbers "lib\functions\fnc_requestClientID.sqf";
DOD_init_hud = compile preprocessFileLineNumbers "lib\init_hud.sqf";

// AI - Artificial Dumbness!
DOD_AI_init = compile preprocessFileLineNumbers "lib\AI\ai_init.sqf";
DOD_fnc_delGroup = compile preprocessFileLineNumbers "lib\functions\fnc_delGroup.sqf"; // redundant? Yes, Yes it is.
DOD_fnc_makeGroupsFromNumber = compile preprocessFileLineNumbers "lib\AI\functions\fnc_makeGroupsFromNumber.sqf";
DOD_fnc_spawnVehicles = compile preprocessFileLineNumbers "lib\AI\functions\fnc_spawnVehicles.sqf";
DOD_fnc_spawnTruck = compile preprocessFileLineNumbers "lib\AI\functions\fnc_spawnTruck.sqf";
DOD_fnc_addUnitEventhandlers = compile preprocessFileLineNumbers "lib\AI\functions\fnc_addUnitEventhandlers.sqf";
DOD_fnc_setUnitProperties = compile preprocessFileLineNumbers "lib\AI\functions\fnc_setUnitProperties.sqf";
DOD_fnc_setUnitSkill = compile preprocessFileLineNumbers "lib\AI\functions\fnc_ai_setUnitSkill.sqf";
DOD_fnc_updateSkillArray = compile preprocessFileLineNumbers "lib\AI\functions\fnc_updateSkillArray.sqf";
DOD_fnc_taskAttack = compile preprocessFileLineNumbers "lib\AI\functions\fnc_taskAttack.sqf";
DOD_fnc_spawnVehiclePlusCrew = compile preprocessFileLineNumbers "lib\AI\functions\fnc_spawnVehiclePlusCrew.sqf";

// BuildBox (BB)
DOD_buildBox_init = compile preprocessFileLineNumbers "lib\logistics\bb_init.sqf";
DOD_BB_openBB = compile preprocessFileLineNumbers "lib\logistics\bb_openGui.sqf";
DOD_BB_fnc_listMaterials = compile preprocessFileLineNumbers "lib\logistics\functions\bb_fnc_listMaterials.sqf";
DOD_BB_addOrder = compile preprocessFileLineNumbers "lib\logistics\functions\bb_fnc_addOrder.sqf";
DOD_bb_fillBox = compile preprocessFileLineNumbers "lib\logistics\functions\bb_fnc_fillBox.sqf"; // redundant!
DOD_BB_fnc_setupMoneyTransfer = compile preprocessFileLineNumbers "lib\logistics\functions\bb_fnc_setupMoneyTransfer.sqf";
DOD_BB_fnc_transferMoney = compile preprocessFileLineNumbers "lib\logistics\functions\bb_fnc_transferMoney.sqf";

// Player-Object-Mover (POM)
DOD_POM_init = compile preprocessFileLineNumbers "lib\logistics\pom_init.sqf";
DOD_POM_moveObjectAction = compile preprocessFileLineNumbers "lib\logistics\pom_moveObject.sqf";
DOD_POM_placeObjectAction = compile preprocessFileLineNumbers "lib\logistics\pom_placeObject.sqf";


// create all the arrays from Config!
execVM "lib\setupArrays.sqf";

// start the Body Removal Service
[] spawn DOD_BodyRemovalService;

// start the helmetcam
[] call DOD_start_helmetCam;


// JIP eventhandler, so that the JIPper get's the corrent values for variables and is in sync with the server
private ["_opc"];
_opc = ["DOD_jipinit_id", "onPlayerConnected", "DOD_JIP_init"] call BIS_fnc_addStackedEventHandler;


// init Farooqs Revive
call compileFinal preprocessFileLineNumbers "FAR_revive\FAR_revive_init.sqf";


// run server scripts
if(isServer) then {
	[] execVM "lib\init_server.sqf";
};

// run player/client scripts
if(!isDedicated) then {
	[] execVM "lib\init_client.sqf";
};


// EOF