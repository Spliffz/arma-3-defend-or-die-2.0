//  Defend Or Die 2.0
//  pick_defPos.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

private ["_pos", "_units", "_shift", "_alt"];

// get params from onmapsingleclick
_pos = _this select 0;
_units = _this select 1; // not used, but here for understanding the function :)
_shift = _this select 2; // not used, but here for understanding the function :)
_alt = _this select 3;


// only do stuff when alt is pressed when the onmapsingleclick happens
if(_alt) then {
	
	if(surfaceIsWater _pos) exitWith { 1337 cutText ["You can not pick a place in water. Please pick some other place.", "PLAIN"];};
	
	// remove onmapsingleclick
	["DOD_eh_onmapclick", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler;
	
	DOD_pv_startup = _pos;
	publicVariableServer "DOD_pv_startup";
	
};


// EOF