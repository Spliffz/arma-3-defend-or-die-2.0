// Defend Or Die 2.0
// Garbage Collector A.K.A Body Removal Service
// 2014, Spliffz

if(!DOD_BodyRemovalService_switch) exitWith {"## DOD2 ## Body Removal System not enabled! Exiting."};

diag_log format ["## DOD2 ## BRS Starting."];

DOD_BRS_removeDead = {
	private ["_garbage"];
	while {DOD_BodyRemovalService_switch} do {
		if(!isDedicated) then {
			// get all the units in a 3000m radius
			_garbage = nearestObjects [position player, ["Man"], 3000]; 
			{
				// delete them if they are dead and further then 10m
				if((!alive _x) && ((_x distance player) > 10)) then { 
					deleteVehicle _x; 
				};
			} foreach _garbage;
			
			sleep 600;
		};
	};
};

/*
DOD_BRS_despawnUnits = {
	private ["_grp", "_dist", "_max"];
	while {DOD_BodyRemovalService_switch} do {
		{
			if(!isDedicated) then {
				if(side _x == RESISTANCE) then {
					//diag_log format ["## DOD2 ## - BRS - DOD_BRS_despawnUnits: _x: %1", _x];
					_grp = _x;
					_x = nil;
					_max = DOD_distance*1.5;
					_dist = player distance (leader _grp);
					//diag_log format ["## DOD2 ## - DOD_fnc_unitDespawn: _dist: %1, _max: %2, _grp: %3, player: %4", _dist, _max, _grp, player];

					if(_dist > _max) then {
						DOD_groupToDel = _grp;
						{
							//diag_log format ["## DOD2 ## - BRS - DOD_BRS_despawnUnits: Despawning unit: %1 from %2", _x, _grp];
							deleteVehicle _x;
						} foreach units _grp;
						deleteGroup _grp;
						publicVariableServer "DOD_groupToDel";
					};
				};
			};
		} foreach allGroups;
	
		sleep 30;
	};
};

[] spawn DOD_BRS_despawnUnits;
*/


[] spawn DOD_BRS_removeDead;


// EOF