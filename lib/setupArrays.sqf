// Defend Or Die 2.0 - BuildBox
// setupArrays.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>


// Keep them local variables private!
private ["_list","_vehicleClass","_faction"];

// --- Opfor Units --- //
// get list from ALL CfgVehicles (big list!)
_list = configFile >> "CfgVehicles";
_faction = DOD_cnf_enemy_faction;

_vehicleClass = "Men";

// loop through _list
for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction)) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_units set [(count DOD_cnf_ai_units), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;


// --- Opfor Men Divers --- //
_vehicleClass = "MenDiver";

// loop through _list
for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction)) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_divers set [(count DOD_cnf_ai_divers), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;


// --- Opfor Ships --- //
_vehicleClass = "Ship";
_badObjects = ["O_Lifeboat"];

// loop through _list
for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction) && !((configName _entry) in _badObjects) ) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_ships set [(count DOD_cnf_ai_ships), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;





// --- Opfor Cars --- //
// get list from ALL CfgVehicles (big list!)
//_list = configFile >> "CfgVehicles";
_vehicleClass = "Car";
//_faction = "OPF_F";
_badObjects = ["O_Quadbike_01_F", "O_Truck_03_device_F", "O_MRAP_02_F"];

for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction) && !((configName _entry) in _badObjects) ) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_cars set [(count DOD_cnf_ai_cars), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;


// --- Opfor Armor --- //
// get list from ALL CfgVehicles (big list!)
//_list = configFile >> "CfgVehicles";
_vehicleClass = "Armored";
//_faction = "OPF_F";
_badObjects = [];

for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction) && !((configName _entry) in _badObjects) ) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_armor set [(count DOD_cnf_ai_armor), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;


// --- Opfor Air --- //
// get list from ALL CfgVehicles (big list!)
//_list = configFile >> "CfgVehicles";
_vehicleClass = "Air";
//_faction = "OPF_F";
_badObjects = ["O_Plane_CAS_02_F"];

for "_i" from 0 to (count (_list)-1) do {
	
	if(isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		//diag_log format ["_entry: %1 - %2", _entry, configName _entry];
		
		if((getText(_entry >> "vehicleClass") == _vehicleClass) && (getText(_entry >> "faction") == _faction) && !((configName _entry) in _badObjects) ) then {
			//diag_log format ["FILTERED: _entry: %1 - %2", _entry, configName _entry];
			
			DOD_cnf_ai_air set [(count DOD_cnf_ai_air), configName _entry];
		};
	};
};

_vehicleClass = nil;
//_faction = nil;

sleep 1;



// --- BuildBox Materials --- //

// get list from ALL CfgVehicles (big list!)
//_list = configFile >> "CfgVehicles";
_vehicleClass = "Fortifications";

// loop through it to filter out only Fortifications
for "_i" from 0 to (count (_list)-1) do {
	
	if (isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;
		
		// extract only usefull objects
		if((configName _entry != "BagFence_base_F") && (configName _entry != "BagBunker_base_F") && (configName _entry != "HBarrier_base_F") && (configName _entry != "fortification")) then {
			if(getText(_entry >> "vehicleClass") == _vehicleClass) then {
				_costs = getNumber (_entry >> "cost");
				if(_costs == 1e+006) then { _costs = 500; }; // this is because BIS hasn't given all the objects proper costs yet.
				
				// put them in array to use
				//DOD_cnf_buildbox_materials set [(count DOD_cnf_buildbox_materials), [configName _entry, (_costs/10)]];
				DOD_cnf_buildbox_materials set [(count DOD_cnf_buildbox_materials), [configName _entry, _costs]];
			};
		};
	};
};


// loop through it to filter out only Military Structures
_vehicleClass = "Structures_Military";
_badObjects = ["BagBunker_base_F", "Cargo_House_base_F", "CamoNet_OPFOR_Curator_F", "CamoNet_INDP_Curator_F", "CamoNet_BLUFOR_open_Curator_F", "CamoNet_OPFOR_open_Curator_F", "CamoNet_INDP_open_Curator_F", "CamoNet_BLUFOR_big_Curator_F", "CamoNet_OPFOR_big_Curator_F", "CamoNet_INDP_big_Curator_F", "Land_TentHangar_V1_F", "Land_TentHangar_V1_dam_F", "CamoNet_OPFOR_F", "CamoNet_INDP_F", "CamoNet_OPFOR_open_F", "CamoNet_INDP_open_F", "CamoNet_OPFOR_big_F", "CamoNet_INDP_big_F", "Land_MilOffices_V1_F", "Land_Cargo_Tower_V3_F", "Land_Cargo_Tower_V2_F", "Land_Cargo_Tower_V1_No7_F", "Land_Cargo_Tower_V1_No6_F", "Land_Cargo_Tower_V1_No5_F", "Land_Cargo_Tower_V1_No4_F", "Land_Cargo_Tower_V1_No3_F", "Land_Cargo_Tower_V1_No2_F", "Land_Cargo_Tower_V1_No1_F", "Land_Cargo_Tower_V1_F", "Cargo_Tower_base_F", "Cargo_Patrol_base_F", "Cargo_HQ_base_F", "Cargo_House_base_F", "Land_i_Barracks_V1_F", "Land_i_Barracks_V1_dam_F", "Land_i_Barracks_V2_F", "Land_i_Barracks_V2_dam_F", "Land_u_Barracks_V2_F", "Shelter_base_F"];
for "_i" from 0 to (count (_list)-1) do {
	
	if (isClass((configFile >> "CfgVehicles") select _i)) then {
		private ["_entry"];
		_entry = (configFile >> "CfgVehicles") select _i;

		// extract only usefull objects
		//if(configName _entry != "fortification") then {
            //_badObjects = ["BagBunker_base_F", "Cargo_House_base_F", "CamoNet_OPFOR_Curator_F", "CamoNet_INDP_Curator_F", "CamoNet_BLUFOR_open_Curator_F", "CamoNet_OPFOR_open_Curator_F", "CamoNet_INDP_open_Curator_F", "CamoNet_BLUFOR_big_Curator_F", "CamoNet_OPFOR_big_Curator_F", "CamoNet_INDP_big_Curator_F", "Land_TentHangar_V1_F", "Land_TentHangar_V1_dam_F", "CamoNet_OPFOR_F", "CamoNet_INDP_F", "CamoNet_OPFOR_open_F", "CamoNet_INDP_open_F", "CamoNet_OPFOR_big_F", "CamoNet_INDP_big_F", "Land_MilOffices_V1_F", "Land_Cargo_Tower_V3_F", "Land_Cargo_Tower_V2_F", "Land_Cargo_Tower_V1_No7_F", "Land_Cargo_Tower_V1_No6_F", "Land_Cargo_Tower_V1_No5_F", "Land_Cargo_Tower_V1_No4_F", "Land_Cargo_Tower_V1_No3_F", "Land_Cargo_Tower_V1_No2_F", "Land_Cargo_Tower_V1_No1_F", "Land_Cargo_Tower_V1_F", "Cargo_Tower_base_F", "Cargo_Patrol_base_F", "Cargo_HQ_base_F", "Cargo_House_base_F", "Land_i_Barracks_V1_F", "Land_i_Barracks_V1_dam_F", "Land_i_Barracks_V2_F", "Land_i_Barracks_V2_dam_F", "Land_u_Barracks_V2_F", "Shelter_base_F"];
            
			if((getText(_entry >> "vehicleClass") == _vehicleClass) && !((configName _entry) in _badObjects) ) then {
                //diag_log format ["## DOD2 ## - setupArrays.sqf - Structures Military - _entry: %1, getText: %2, cfgName: %3", _entry, getText(_entry >> "vehicleClass"), configName (_entry)];
                
				_costs = getNumber (_entry >> "cost");
				if(_costs == 1e+006) then { _costs = 500; }; // this is because BIS hasn't given all the objects proper costs yet.
				
				// put them in array to use
				//DOD_cnf_buildbox_materials set [(count DOD_cnf_buildbox_materials), [configName _entry, (_costs/10)]];
				DOD_cnf_buildbox_materials set [(count DOD_cnf_buildbox_materials), [configName _entry, _costs]];
			};
		//};
	};
};



// EOF