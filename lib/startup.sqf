// Defend Or Die 2.0
// startup.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

// don't run this on clients
if(!isServer) exitWith {};

// get data from publicvariable
_params = _this select 0;
_pos = _params select 1;
//diag_log format ["## DOD ## - startup.sqf - _params: %1, _pos: %1", _params, _pos];

// create marker for future referece
DOD_mrk_defPos = createMarker ["DOD_mrk_defPos", _pos];

// reposition players
{ 
	if(isPlayer _x) then {
		_x setPos _pos;
	};
} foreach allUnits;

// spawn supplybox for the fortification materials
DOD_obj_buildBox = "B_SupplyCrate_F" createVehicle _pos;
DOD_VAS_init = [DOD_obj_buildBox];
[DOD_VAS_init, "DOD_fnc_init_VAS", true, true] call BIS_fnc_MP;
DOD_obj_buildBox allowDamage false;

// spawn helipad (to inform the players where the stuff spawns once ordered)
DOD_obj_spawnpoint = "Land_HelipadSquare_F" createVehicle [(_pos select 0)+8, (_pos select 1)+8, (_pos select 2)];

// run BuildBox script with the just created crate as material spawnpoint
[DOD_obj_buildBox, "DOD_buildBox_init", true, true] call BIS_fnc_MP;

// start base-build timer - xx minutes to build your base!
[DOD_startTimer, "DOD_timer", true, true] call BIS_fnc_MP;


// EOF