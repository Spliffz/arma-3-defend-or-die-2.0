//  Defend Or Die 2.0
//  init_client.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

// if box is dedicated, don't run this script
if(isDedicated) exitWith {};
if!(local player) exitWith {};

// player eventhandlers
"DOD_pvc_n_enemyKilled" addPublicVariableEventHandler {
	private ["_data", "_unitname", "_score", "_playerMoney", "_kills", "_hud", "_ui"];
	_data = _this select 1;
	_unitname = _data select 0;
	_score = _data select 1;
	_playerMoney = player getVariable ["DOD_BankAccount", 0];
	player setVariable ["DOD_BankAccount", (_playerMoney+_score), false];

    // add +1 to player kill counter
    _kills = player getVariable ["DOD_stats_killed", 0];
    player setVariable ["DOD_stats_killed", (_kills+1), true];
    
    // update HUD - stats
    _ui = uiNamespace getVariable ["dod_gui_hud", false];
    _hud = _ui displayCtrl 1110;
    _hud ctrlSetStructuredText parseText format ["<t align='right'>Your Kills: %1</t>", (_kills+1)];
    // update HUD - points
    _ui = uiNamespace getVariable ["dod_gui_hud", false];
    _hud = _ui displayCtrl 1113;
    _hud ctrlSetStructuredText parseText format ["<t align='right'>Your Points: %1</t>", (_playerMoney+_score)];
    
	player sideChat format ["You killed an enemy! +%1", str _score];
};

"DOD_pvc_n_enemyKilled_headshot" addPublicVariableEventHandler {
	private ["_data", "_unitname", "_score", "_playerMoney", "_kills"];
	_data = _this select 1;
	_unitname = _data select 0;
	_score = _data select 1;
	_playerMoney = player getVariable ["DOD_BankAccount", 0];
	player setVariable ["DOD_BankAccount", (_playerMoney+_score), false];

	player sideChat format ["You killed an enemy with a headshot! +%1", str _score];
};

"DOD_pvc_n_enemyCarKilled" addPublicVariableEventHandler {
	private ["_data", "_unitname", "_score", "_playerMoney"];
	_data = _this select 1;
	_unitname = _data select 0;
	_score = _data select 1;
	_playerMoney = player getVariable ["DOD_BankAccount", 0];
	player setVariable ["DOD_BankAccount", (_playerMoney+_score), false];
	
	player sideChat format ["You killed an enemy vehicle! +%1", str _score];
};

"DOD_pvc_transferMoney" addPublicVariableEventHandler {
    diag_log format ["## DOD2 ## - init_client.sqf: DOD_pvc_transferMoney - _this: %1", _this];
    _data = _this select 1;
    _money = _data select 0;
    _from = _data select 1;
	_playerMoney = player getVariable ["DOD_BankAccount", 0];
	player setVariable ["DOD_BankAccount", (_playerMoney+_money), false];
	player sideChat format ["You have got send %1 from %2.", str _score, _from];
};


// the awesome selfheal addaction!
// this is so you can get yourself up if you are playing alone!
// integrate this with Farooqs revive!
player addEventHandler ["Hit", {
		DOD_p_selfHealAction = player addAction ["Heal Yourself", { 
			//player switchMove "";
			player setDamage 0;
			//player removeItem "FirstAidKit";
			player removeAction (_this select 2);
		}, "", 6, true, true, "", "isNil 'DOD_p_selfHealAction' && 'FirstAidKit' in (items player)"];
	}
];

/* --- Inline Functions --- */
DOD_n_startAttackNotification = {
	["DOD_n_startAttack", []] call BIS_fnc_showNotification;
};


/* --- The Code --- */

// wait until player is an object
waitUntil {!isNull player};

// set clientID to each player
DOD_pvs_requestClientID = player;
publicVariableServer "DOD_pvs_requestClientID";

// if hosted session
if(isServer) then { 
    [0, player] call DOD_fnc_requestClientID;
};


// remove weapons from new spawned players, to ensure no 'misfires'
removeAllWeapons player;

// add start money to player bank 
player setVariable ["DOD_BankAccount", 1000, false];


// initialize HUD
call DOD_init_hud;


// if player is the choosen one, then present him with option to choose the Defend Position
if(player == dod_chooser) then {
	
	// intro text 
	//1337 cutText [format ["Hi %1! Today is your lucky day! Today you can pick the place to defend! 'How?' You ask?! That's simple: Open your map and alt+click somewhere on it. It's really that simple!", (name player)], "PLAIN", 10, true];
	1337 cutText [format ["Hi %1! Please open your map and alt+click somewhere to start the game.", (name player)], "PLAIN", 1, true];
	
	
	//add stacked eventhandlers for onmapsingleclick - this chooses the location.
	["DOD_eh_onmapclick", "onMapSingleClick", { _this call DOD_pick_defPos; } ] call BIS_fnc_addStackedEventHandler;
};


// relocate JIPper to defend position
if(player getVariable ["isJip", false]) then {
    player setPos (getMarkerPos "DOD_mrk_defPos");
};


// return true, to end script
true


// EOF