// Defend Or Die 2.0
// defines.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>


// This enables/disables the BRS (GarbageCollector)
DOD_BodyRemovalService_switch = true;


// The color of the BuildBox addAction menu item - Change this if you want
DOD_cnf_actionMenu_color = "#FCF94B"; 

/* -- DO NOT EDIT THE VALUES BELOW -- */

// Enemy side & Faction
// If you edit this, make sure you put down the right faction!
DOD_cnf_enemy_side = EAST;
DOD_cnf_enemy_faction = "OPF_F";

// Don't edit this. This value has to be 25, or the whole game stops working proper!
DOD_round_max = 25; 

// DO NOT EDIT! This is dynamicly created by BuildBox
DOD_cnf_buildbox_materials = [];

// AI stuff - DO NOT EDIT! dynamically created
DOD_cnf_ai_units = [];
DOD_cnf_ai_cars = [];
DOD_cnf_ai_air = [];
DOD_cnf_ai_armor = [];
DOD_cnf_ai_divers = []; 
DOD_cnf_ai_ships = []; 
DOD_cnf_ai_submarines = []; // not used yet
DOD_cnf_ai_statics = [];
DOD_grpArray = [];

// DON'T EDIT THIS! These are starting values which are build up every round
DOD_ai_skill_array = 
[
    ["aimingAccuracy", 0.1],
    ["aimingShake", 0.1],
    ["aimingSpeed", 0.1],
    ["endurance", 0.1],
    ["spotDistance", 0.1],
    ["spotTime", 0.1],
    ["courage", 0.1],
    ["reloadSpeed", 0.1],
    ["commanding", 0.1],
    ["general", 0.1]
];




// EOF