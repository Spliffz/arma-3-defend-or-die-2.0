// Defend Or Die 2.0
// cfg_notifications.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

	/* Example
	class Default
	{
		title = ""; // Tile displayed as text on black background. Filled by arguments.
		iconPicture = "\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa"; // Small icon displayed in left part. Colored by "color", filled by arguments.
		iconText = ""; // Short text displayed over the icon. Colored by "color", filled by arguments.
		description = ""; // Brief description displayed as structured text. Colored by "color", filled by arguments.
		color[] = {1,1,1,1}; // Icon and text color
		duration = 5; // How many seconds will the notification be displayed
		priority = 0; // Priority; higher number = more important; tasks in queue are selected by priority
		difficulty[] = {}; // Required difficulty settings. All listed difficulties has to be enabled
	};
	*/
	
	class DOD_n_startAttack
	{
		title = "They've found the base!";
		iconPicture = "lib\media\DoD_icon.paa";
		iconText = "";
		description = "Get ready! Opfor attack imminent";
		color[] = {1,1,1,1};
		duration = 3;
		priority = 1;
	};
	
	class DOD_n_nextRound
	{
		title = "Next Wave";
		iconPicture = "lib\media\DoD_icon.paa";
		iconText = "";
		description = "Wave %1";
		color[] = {1,1,1,1};
		duration = 3;
		priority = 1;
	};

	
// EOF