//-------------------------------------
//	Defend Or Die 2.0
//	by Spliffz <theSpliffz@gmail.com>
//	lib\config\cfg_params.sqf - 2013
//-------------------------------------

	class DOD_p_timedate_menu {
		title = "Time & Date:";
		values[] = {0,0};
		texts[] = {"",""};
		default = 0;			
		code = "DOD_p_timedate_menu = %1";
	};
	
	class DOD_p_timedate_random {
		title = "  Random Time & Date?";
		values[] = {1,0};
		texts[] = {"Yes","No - config below"};
		default = 0;
		code = "DOD_p_timedate_random = %1";
	};
	
	class DOD_p_timedate_month { 	
		title = "  Month"; 
		values[] = {1,2,3,4,5,6,7,8,9,10,11,12}; 
		texts[] = {"January","February","March","April","May","June","July","August","September","October","November","December"}; 
		default = 8; 
		code = "DOD_p_timedate_month = %1";
	}; 
	class DOD_p_timedate_day { 
		title = "  Day"; 
		values[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}; 
		texts[] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"}; 
		default = 14; 
		code = "DOD_p_timedate_day = %1";
	}; 
	class DOD_p_timedate_hour { 
		title = "  Hour"; 
		values[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}; 
		texts[] = { "0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"}; 
		default = 14; 
		code = "DOD_p_timedate_hour = %1";
	}; 
	class DOD_p_timedate_minutes { 
		title = "  Minutes"; 
		values[] = {00,05,10,15,20,25,30,35,40,45,50,55}; 
		texts[] = {"00","05","10","15","20","25","30","35","40","45","50","55"}; 
		default = 00; 
		code = "DOD_p_timedate_minutes = %1";
	};
    
    
	class DOD_p_BAS_menu {
		title = "Enemy Parameters (# per wave):";
		values[] = {0,0};
		texts[] = {"",""};
		default = 0;			
		code = "EmptyLine2 = %1";
	};
    /*
	class DOD_p_BAS_spawnDistance { 
		title = "  Spawn Distance"; 
		values[] = {300,400,500,600,700,800,900,1000}; 
		texts[] = {"300","400","500","600","700","800","900","1000"}; 
		default = 400; 
		code = "BAS_spawnDistance = %1";
	};
    */
	class DOD_p_BAS_enemySide { 
		title = "  Enemy side"; 
		values[] = {east,resistance}; 
		texts[] = {"East", "Independent"};
		default = 0; 
		code = "BAS_enemySide = %1";	
	};
	class DOD_p_BAS_enemyFaction { 
		title = "  Enemy Faction"; 
		values[] = {east,resistance}; 
		texts[] = {"EAST - Opfor - CSAT", "EAST - IND"};
		default = 0; 
		code = "BAS_enemySide = %1";	
	};
	
    
	class DOD_p_misc_menu {
		title = "Other Options:";
		values[] = {0,0};
		texts[] = {"",""};
		default = 0;			
		code = "DOD_p_misc_menu = %1";
	};
	class DOD_p_misc_helmetCam { 
		title = "  Helmet Cam"; 
		values[] = {1,2}; 
		texts[] = {"Yes","No",}; 
		default = 1; 
		code = "DOD_p_misc_helmetCam = %1";	
	};
    
	
// EOF