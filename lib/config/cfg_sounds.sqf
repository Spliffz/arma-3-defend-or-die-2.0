// Defend Or Die 2.0
// cfg_sounds.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

	sounds[] = {};

	class DOD_siren
	{
		name = "DOD_siren"; // Name for mission editor
		sound[] = {lib\sounds\siren.ogg, db + 0, 1};
		titles[] = {0, ""};
	};


// EOF