//  Defend Or Die 2.0
//  fnc_init_vas.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

// run on everything except a dedicated box.
//if(isDedicated) exitWith {};

// keep them local variables private!
private ["_params", "_paramsArr"];

// get data from params
_paramsArr = [];

// make an array from the params, if it isn't one
if(typeName _this == "ARRAY") then {
    _paramsArr = _this;
} else {
    _paramsArr = [];
    {
        _paramsArr set [(count _paramsArr), _x];
    } foreach units _params;
};

// set _this to nothing, to ensure it can be re-used in the code below.
_this = nil;

// set VAS to each object
{
    _x addAction["<t color='#ff1111'>Virtual Ammobox</t>", "VAS\open.sqf"];
} foreach _paramsArr;


// return true to end script
true


// EOF