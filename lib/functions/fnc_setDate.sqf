//-------------------------------------
//	co16 Defend Or Die 2.0
//
//	by Spliffz <theSpliffz@gmail.com>
//	lib\functions\fnc_setDate.sqf - 2013
//
// 	last edit: Spliffz, march 2014
//-------------------------------------

private ["_month", "_day", "_hour", "_minutes", "_year"];

if(DOD_p_timedate_random == 1) then {
	_month = ceil (round (random 12));
	_day = ceil (round (random 30));
	_hour = round (random 23);
	_minutes = round (random 59);
	_year = 2035;
	setDate [_year, _month, _day, _hour, _minutes];
} else {
	_month = if!(isNil "DOD_p_timedate_month") then { DOD_p_timedate_month; } else { random 12; };
	_day = if!(isNil "DOD_p_timedate_day") then { DOD_p_timedate_day; } else { random 31; };
	_hour = if!(isNil "DOD_p_timedate_hour") then { DOD_p_timedate_hour; } else { random 24; };
	_minutes = if!(isNil "DOD_p_timedate_minutes") then { DOD_p_timedate_minutes; } else { random 60; };
	_year = 2035;
	setDate [_year, _month, _day, _hour, _minutes];
};


// EOF