// Defend Or Die 2.0
// fnc_delGroup.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

//ZP_z_FNC_delGroup = {
	private ["_grp"];
	_grp = _this select 0;
	if(DOD_debug) then {
		diag_log format ["## ZP ## - fnc_server.sqf - FNC: ZP_z_FNC_delGroup: _grp: %1", _grp];
	};
	{
		//diag_log format ["## ZP ## - BRS - ZP_z_BRS_despawnUnits: Despawning unit: %1 from %2", _x, _grp];
		deleteVehicle _x;
	} foreach units _grp;
	if(DOD_debug) then {
		diag_log format ["## ZP ## - BRS - ZP_z_BRS_despawnUnits: Deleting group: %1", _grp];
	};
	deleteGroup _grp;
	if(DOD_debug) then {
		diag_log format ["## ZP ## - BRS - ZP_z_BRS_despawnUnits: Deleted group: %1", _grp];
	};
	_grp = grpNull;
//};


// EOF