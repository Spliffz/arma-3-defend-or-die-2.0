// Defend Or Die 2.0
// fnc_requestClientID.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

// exit when not server
if(!isServer) exitWith {};

// keep them local vars private!
private ["_player"];

// get data from params
_player = _this select 1;

// set clientID as player variable so that we can use it later as clients
_player setVariable ["DOD_clientID", owner _player, true];


// return true to end script
true


// EOF