//  Defend Or Die 2.0
//  parseParameters.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

// get parameters
for [ { _i = 0 }, { _i < count(paramsArray) }, { _i = _i + 1 } ] do
{
	_paramName =(configName ((missionConfigFile >> "Params") select _i));
	_paramValue = (paramsArray select _i);
	_paramCode = (getText (missionConfigFile >> "Params" >> _paramName >> "code"));
	_code = format [_paramCode, _paramValue];
	
	call compile _code;
};


// EOF