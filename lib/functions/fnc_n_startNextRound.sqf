// Defend Or Die 2.0
// fnc_n_startNextRound.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

if(isDedicated) exitWith {};

private ["_round"];

// get params
_round = _this select 0;

// show the notification
["DOD_n_nextRound", [_round]] call BIS_fnc_showNotification;

// WHOOOOOOOooooooooooooo
playSound "DOD_siren";


// return 
true

// EOF