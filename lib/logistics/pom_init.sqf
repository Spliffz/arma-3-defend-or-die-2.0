// Defend Or Die 2.0 - Player-Object-Mover (POM)
// pom_init.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private ["_obj"];

// get data from params
_obj = _this;
_this = nil;

// add addaction to the object
_obj addAction ["Move Object", {[_this] spawn DOD_POM_moveObjectAction; }, "", 5, true, true, "", ""];


// EOF