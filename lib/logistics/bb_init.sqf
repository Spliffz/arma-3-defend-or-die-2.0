// Defend Or Die 2.0 - BuildBox
// bb_init.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>


// include the defines
#include "bb_defines.sqf"

// keep them local variables private!
private ["_obj"];

// get data from parameters
_obj = _this;

// make _this non-existent
_this = nil;

// addaction on the object
DOD_BB_action_openbb = _obj addAction [format ["<t color='%1'>Open BuildBox</t>", DOD_cnf_actionMenu_color], {[_this] spawn DOD_BB_openBB; }, "", 5, true, true, "", ""];


// EOF