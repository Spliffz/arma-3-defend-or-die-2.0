// Defend Or Die 2.0 - Player-Object-Mover (POM)
// pom_moveObject.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private ["_obj","_caller","_id","_type"];

//diag_log format ["## DOD2 POM ## - pom_moveObject.sqf - _this: %1", _this];

// get data from params;
_this = _this select 0;
_obj = _this select 0;
_caller = _this select 1;
_id = _this select 2;

// delete object and spawn new one for moving
deleteVehicle _obj;
_type = typeOf _obj;
_obj = _type createVehicle (getPosATL player);

// attach to player, in front of player, set dir player
//_obj setPos (player modelToWorld [0,6,0]);
_obj setDir ([_obj, player] call BIS_fnc_dirTo);

_obj attachTo [player, [0, (((boundingBox _obj select 1 select 1) max (-(boundingBox _obj select 0 select 1))) max ((boundingBox _obj select 1 select 0) max (-(boundingBox _obj select 0 select 0)))) + 1,	1]];

// addAction for placing the object
player addAction ["Place Object", {[_this] spawn DOD_POM_placeObjectAction; }, [_obj], 5, true, true, "", ""];

// end script
true


// EOF