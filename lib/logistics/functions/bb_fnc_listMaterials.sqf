// Defend Or Die 2.0 - BuildBox
// functions\bb_fnc_listMaterials.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private [];

// clear the listbox
lbClear 1500;

// loop through the material list and add them to the listbox 
for "_i" from 0 to (count (DOD_cnf_buildbox_materials)-1) do {
	private ["_arr", "_item", "_remaining", "_text", "_cost", "_name"];
	
	// get the items from the array
	_arr = DOD_cnf_buildbox_materials select _i;
	_item = _arr select 0;
	//_remaining = _arr select 1;
	_cost = _arr select 1;
	_name = getText (configFile >> "CfgVehicles" >> _item >> "displayName");
	// format a string for the name in the listbox
	_text = format ["%1 (Costs: %2pt)", _name, _cost];
	
	// add it to the listbox
	lbAdd [1500, _text];
	
	// and set the item data so it knows what to pick on selection
	lbSetData [1500, ((lbSize 1500)-1), _item];
};


// EOF