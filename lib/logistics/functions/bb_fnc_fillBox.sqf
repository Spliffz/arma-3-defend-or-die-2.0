// Defend Or Die 2.0 - BuildBox
// bb_fnc_fillBox.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// get list from ALL CfgVehicles (big list!)
_list = configFile >> "CfgVehicles";

// loop through it to filter out only Fortifications
for "_i" from 0 to (count (_list)-1) do {
	
	if (isClass((configFile >> "CfgVehicles") select _i)) then {
		_entry = (configFile >> "CfgVehicles") select _i;
		
		// extract only usefull objects
		if((configName _entry != "BagFence_base_F") && (configName _entry != "BagBunker_base_F") && (configName _entry != "HBarrier_base_F") && (configName _entry != "fortification")) then {
			if(getText(_entry >> "vehicleClass") == "Fortifications") then {
				_costs = getNumber (_entry >> "cost");
				if(_costs == 1e+006) then { _costs = 500; }; // this is because BIS hasn't given all the objects proper costs yet.
				
				// put them in array to use
				DOD_cnf_buildbox_materials set [(count DOD_cnf_buildbox_materials), [configName _entry, (_costs/10)]];
			};
		};
	};
};


// EOF	