// Defend Or Die 2.0 - BuildBox
// functions\bb_fnc_transferMoney.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private ["_selectedItem", "_itemData", "_arr", "_id", "_itemArray", "_av", "_fort"];

// get the selected user from listbox
_selectedItem = lbCurSel 2100;
_itemData = lbData [2100, (lbCurSel 2100)];
diag_log format ["## DOD2 ## bb_fnc_transferMoney.sqf - _selectedItem: %1, _itemData: %2", _selectedItem, _itemData];

// get ammount of money to send from listbox
_moneyAmount = lbCurSel 2101;
_moneyAmountData = lbData [2101, (lbCurSel 2101)];
diag_log format ["## DOD2 ## bb_fnc_transferMoney.sqf - _moneyAmount: %1, _moneyAmountData: %2", _moneyAmount, _moneyAmountData];


// transfer money
// retract current player money
_playerMoney = player getVariable ["DOD_BankAccount", 0];
player setVariable ["DOD_BankAccount", (_playerMoney-_moneyAmountData), false];


// pubvar the transfer
DOD_pvc_transferMoney = [_moneyAmountData, name player];
_itemData publicVariableClient "DOD_pvc_transferMoney";


// inform the player of the transfer
player sideChat format ["You just transferred %1 to %2", _moneyAmount, _selectedItem];


// EOF