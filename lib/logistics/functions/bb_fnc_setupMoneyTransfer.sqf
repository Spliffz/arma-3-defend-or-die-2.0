// Defend Or Die 2.0 - BuildBox
// functions\bb_fnc_setupMoneyTransfer.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>


// list players
{
    if((isPlayer _x) && (alive _x)) then {
        lbAdd [2100, name _x];
        lbSetData [2100, ((lbSize 2100)-1), (_x getVariable ["DOD_clientID", false])];
        diag_log format ["## DOD2 ## - bb_fnc_setupMoneyTransfer.sqf - name: %1, id: %2", (name _x),  (_x getVariable ["DOD_clientID", false])];
    };
} foreach allUnits;

// list money amount 
{
    lbAdd [2101, str _x];
    lbSetData [2101, ((lbSize 2101)-1), _x];
} foreach [100,200,300,400,500,1000];


true

// EOF