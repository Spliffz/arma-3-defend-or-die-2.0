// Defend Or Die 2.0 - BuildBox
// functions\bb_fnc_addOrder.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private ["_selectedItem", "_itemData", "_arr", "_id", "_itemArray", "_av", "_fort"];

// get the selected item from listbox
_selectedItem = lbCurSel 1500;
_itemData = lbData [1500, (lbCurSel 1500)];
_playerMoney = player getVariable ["DOD_BankAccount", 0];

// get the corresponding item from the materials array
_arr = [DOD_cnf_buildbox_materials, _itemData] call BIS_fnc_findNestedElement;
_id = _arr select 0;
_itemArray = DOD_cnf_buildbox_materials select _id;

// do buy check, see if you have the money
if((_playerMoney - (_itemArray select 1)) >= 0) then {
	// buy it, thus retract money from bank
	_playerMoney = _playerMoney - (_itemArray select 1);
	player setVariable ["DOD_BankAccount", _playerMoney, false];
    
	// set the buildbox gui info again, refresh it so it displays the new values
	[] call DOD_BB_fnc_listMaterials;
	//ctrlSetText [1001, format ["%1", DOD_BB_totalmoney]];
	ctrlSetText [1001, format ["%1", _playerMoney]];
	
	// spawn the object on the server
	DOD_pv_createObject = _itemData;
	publicVariableServer "DOD_pv_createObject";
} else {
	hint "You do not have enough money to buy this.";
};


// EOF