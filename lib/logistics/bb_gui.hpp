// Defend Or Die 2.0
// bb_gui.hpp
// 2014 - Spliffz <theSpliffz@gmail.com>


////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Spliffz, v1.063, #Soxeco)
////////////////////////////////////////////////////////
class dod_buildbox_gui 
{
	
	idd = 3100;
	movingEnable = 1;
	
	class Controls 
	{

        class dod_bb_background: IGUIBack
        {
            idc = 2200;
            x = 0.340156 * safezoneW + safezoneX;
            y = 0.302 * safezoneH + safezoneY;
            w = 0.33 * safezoneW;
            h = 0.363 * safezoneH;
        };
        class dod_bb_itemLB: RscListbox
        {
            idc = 1500;
            x = 0.350469 * safezoneW + safezoneX;
            y = 0.379 * safezoneH + safezoneY;
            w = 0.309375 * safezoneW;
            h = 0.242 * safezoneH;
        };
        class dod_bb_button_order: RscButton
        {
            idc = 1600;
            text = "Order"; //--- ToDo: Localize;
            x = 0.463906 * safezoneW + safezoneX;
            y = 0.632 * safezoneH + safezoneY;
            w = 0.0773437 * safezoneW;
            h = 0.022 * safezoneH;
			action = "0 call DOD_BB_addOrder";
        };
        class dod_bb_titletext: RscStructuredText
        {
            idc = 1100;
            text = "BuildBox"; //--- ToDo: Localize;
            x = 0.479375 * safezoneW + safezoneX;
            y = 0.313 * safezoneH + safezoneY;
            w = 0.0567187 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_bankText: RscText
        {
            idc = 1000;
            text = "Bank: "; //--- ToDo: Localize;
            x = 0.350469 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0309375 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_bankAmount: RscText
        {
            idc = 1001;
            text = "5000"; //--- ToDo: Localize;
            x = 0.381406 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0515625 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_transferMoney_text: RscText
        {
            idc = 1003;
            text = "Transfer:"; //--- ToDo: Localize;
            x = 0.438125 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0464063 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_transferMoney_textto: RscText
        {
            idc = 1002;
            text = "to"; //--- ToDo: Localize;
            x = 0.54125 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0154688 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_transferMoney_button: RscButton
        {
            idc = 1601;
            text = "Send"; //--- ToDo: Localize;
            x = 0.628906 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0309375 * safezoneW;
            h = 0.022 * safezoneH;
			action = "0 call DOD_BB_fnc_transferMoney";
        };
        class dod_bb_transferMoney_listmoney: RscCombo
        {
            idc = 2100;
            x = 0.484531 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0567187 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_bb_transferMoney_userlist: RscCombo
        {
            idc = 2101;
            x = 0.556719 * safezoneW + safezoneX;
            y = 0.346 * safezoneH + safezoneY;
            w = 0.0670312 * safezoneW;
            h = 0.022 * safezoneH;
        };
    };
};

////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////

// EOF