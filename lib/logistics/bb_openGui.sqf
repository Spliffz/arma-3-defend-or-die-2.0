// Defend Or Die 2.0 - BuildBox
// bb_openGui.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private [];


// open the dialog
null = createDialog "dod_buildbox_gui";

// set bank amount
ctrlSetText [1001, format ["%1", (player getVariable ["DOD_BankAccount", 0])]];

// put the data in it
[] call DOD_BB_fnc_listMaterials;

[] call DOD_BB_fnc_setupMoneyTransfer;




// EOF