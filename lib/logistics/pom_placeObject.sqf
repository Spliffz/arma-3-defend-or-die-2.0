// Defend Or Die 2.0 - Player-Object-Mover (POM)
// pom_moveObject.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// keep them local variables private!
private ["_obj","_pos","_type","_item", "_praid", "_itemPos", "_playerPos", "_itemNewPos", "_itemDir"];

// get data from params
_this = _this select 0;
_obj = _this select 3 select 0;
_praid = _this select 2;
_this = nil;

// get pos from moving object
_pos = getPosASL _obj;
_type = typeOf _obj;

detach _obj;
deleteVehicle _obj;

// create object on new pos
//_item = _type createVehicle _pos;
_item = createVehicle [_type, _pos, [], 0, "CAN_COLLIDE"];

// place the object
_itemPos = getPosASL _item;
_playerPos = getPosASL player;
_itemPos set [2, _playerPos select 2];
_itemNewPos = [] + _itemPos;
_itemNewPos set [2, (_itemNewPos select 2) - 1];
_itemDir = direction _item;
if(terrainIntersectASL [_itemPos, _itemNewPos]) then {
	//diag_log format ["## DOD2 ## - pom_placeObject.sqf - TerrainIntersect: Yes"];
	_item setPosATL [(position player select 0)-5*sin((direction player)-180),(position player select 1)-5*cos((direction player)-180), 0];
} else {
	//diag_log format ["## DOD2 ## - pom_placeObject.sqf - TerrainIntersect: No"];
	_item setPosATL [(position player select 0)-5*sin((direction player)-180),(position player select 1)-5*cos((direction player)-180), 0];
	//_item setPosATL [(_itemPos select 0), (_itemPos select 1), 0];
};


// set correct direction of the object
_item setDir ([_item, player] call BIS_fnc_dirTo);

// remove place action
player removeAction _praid;


// set delete action
_item addAction ["Delete Object", { deleteVehicle (_this select 0); }, "", 5, true, true, "", ""];


// end script
true


// EOF