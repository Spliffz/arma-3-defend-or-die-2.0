// Defend Or Die 2.0
// dod_gui.hpp
// 2014 - Spliffz <theSpliffz@gmail.com>

class dod_gui
{
    idd = 3101;
    enableSimulation = 1;
    duration = 1.0e+11;
    name = "dod_gui_hud";
    onLoad = "uiNamespace setVariable ['dod_gui_hud', _this select 0]";
    
    class Controls
    {
        /*
        class dod_logo: RscPicture
        {
            idc = 1200;
            text = "lib\media\dod_icon.paa";
            x = 0.953647 * safezoneW + safezoneX;
            y = 0.93714 * safezoneH + safezoneY;
            w = 0.04125 * safezoneW;
            h = 0.055 * safezoneH;
        };
        class dod_gui_total_kills: RscStructuredText
        {
            idc = 1110;
            text = "Total Kills: 0"; //--- ToDo: Localize;
            x = 0.9125 * safezoneW + safezoneX;
            y = 0.907 * safezoneH + safezoneY;
            w = 0.0825 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_gui_player_kills: RscStructuredText
        {
            idc = 1111;
            text = "Your Kills: 0"; //--- ToDo: Localize;
            x = 0.9125 * safezoneW + safezoneX;
            y = 0.874 * safezoneH + safezoneY;
            w = 0.0825 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_gui_round: RscStructuredText
        {
            idc = 1112;
            text = "ROUND 10"; //--- ToDo: Localize;
            x = 0.443281 * safezoneW + safezoneX;
            y = 0.00500001 * safezoneH + safezoneY;
            w = 0.0928125 * safezoneW;
            h = 0.044 * safezoneH;
        };
        */

        class dod_logo: RscPicture
        {
            idc = 1200;
            text = "lib\media\dod_icon.paa";
            x = 0.953647 * safezoneW + safezoneX;
            y = 0.93714 * safezoneH + safezoneY;
            w = 0.04125 * safezoneW;
            h = 0.055 * safezoneH;
        };
        class dod_gui_player_kills: RscStructuredText
        {
            idc = 1110;
            text = "Your Kills: 26"; //--- ToDo: Localize;
            x = 0.9125 * safezoneW + safezoneX;
            y = 0.874 * safezoneH + safezoneY;
            w = 0.0825 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_gui_total_kills: RscStructuredText
        {
            idc = 1111;
            text = "Total Kills: 54"; //--- ToDo: Localize;
            x = 0.9125 * safezoneW + safezoneX;
            y = 0.841 * safezoneH + safezoneY;
            w = 0.0825 * safezoneW;
            h = 0.022 * safezoneH;
        };
        class dod_gui_round: RscStructuredText
        {
            idc = 1112;
            text = "ROUND 10"; //--- ToDo: Localize;
            x = 0.443281 * safezoneW + safezoneX;
            y = 0.00500001 * safezoneH + safezoneY;
            w = 0.0928125 * safezoneW;
            h = 0.044 * safezoneH;
        };
        class dod_gui_player_money: RscStructuredText
        {
            idc = 1113;
            text = "Points: 1000"; //--- ToDo: Localize;
            x = 0.9125 * safezoneW + safezoneX;
            y = 0.907 * safezoneH + safezoneY;
            w = 0.0825 * safezoneW;
            h = 0.022 * safezoneH;
        };
        
    };
};


// EOF