// Defend Or Die 2.0
// fnc_ai_setUnitSkill.sqf
// 2014 - Spliffz <theSpliffz@gmail.com


// keep them local variables private!
private ["_ai_skill_to_add", "_countSkillsArray", "_grp"];

// divide 1 by the max nubmer of rounds, to get the proper skill points to add every round
_ai_skill_to_add = 1/DOD_round_max;

/*
DOD_ai_skill_array = 
[
    ["aimingAccuracy", 0.1],
    ["aimingShake", 0.1],
    ["aimingSpeed", 0.1],
    ["endurance", 0.1],
    ["spotDistance", 0.1],
    ["spotTime", 0.1],
    ["courage", 0.1],
    ["reloadSpeed", 0.1],
    ["commanding", 0.1],
    ["general", 0.1]
];
*/

// get data from params
_grp = _this select 0;
_countSkillsArray = count (DOD_ai_skill_array select 0); 

// set unit skills
{
    for "_i" from 0 to _countSkillsArray-1 do {
        private ["_skill", "_arrItem", "_arrItemSkill", "_newSkill"];
        _skill = DOD_ai_skill_array select _i;
        //diag_log format ["## DOD2 ## - fnc_ai_setUnitSkill.sqf - _skill: %1", _skill];
        
        _arrItem = _skill select 0;
        //diag_log format ["## DOD2 ## - fnc_ai_setUnitSkill.sqf - _arrItem: %1", _arrItem];

        _arrItemSkill = _skill select 1;
        //diag_log format ["## DOD2 ## - fnc_ai_setUnitSkill.sqf - _arrItemSkill: %1", _arrItemSkill];

        _newSkill = _arrItemSkill+_ai_skill_to_add;
        //diag_log format ["## DOD2 ## - fnc_ai_setUnitSkill.sqf - _newSkill: %1", _newSkill];

        _x setSkill [_arrItem, _newSkill];
    };
} foreach _grp;


// return true to end script
true


// EOF