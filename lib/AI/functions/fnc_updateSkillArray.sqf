// Defend Or Die 2.0
// fnc_updateSkillArray.sqf
// 2014 - Spliffz <theSpliffz@gmail.com


// keep them local variables private!
private ["_ai_skill_to_add", "_countSkillsArray"];

// divide 1 by the max nubmer of rounds, to get the proper skill points to add every round
_ai_skill_to_add = 1/DOD_round_max;

// count array
_countSkillsArray = count (DOD_ai_skill_array select 0); 

// update skill array
for "_i" from 0 to _countSkillsArray-1 do {
    private ["_skill"];
    _skill = DOD_ai_skill_array select _i;
    //diag_log format ["## DOD2 ## - fnc_updateSkillArray.sqf - _skill: %1", _skill];
    
    // set skills array with new correct values
    _skill set [1, (_skill select 1)+_ai_skill_to_add];
};


// return true to end script
true


// EOF