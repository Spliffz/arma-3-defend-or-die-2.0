// Defend Or Die 2.0
// fnc_makeGroupsFromNumber.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

/* 
	[grpsize(int), totalunits(int), side(str), [spawnpos (3DF), maxdistance(int)]]
	spawn groups of units divided by _n.
	also, gives each grp it's own spawn location around the AO.
*/

// exit script on clients
if(!isServer) exitWith {};

private ["_n", "_totalUnits", "_spawnPos", "_grp", "_unit", "_rest", "_side", "_posArray", "_DOD_grpArray", "_DOD_grpArrayInput", "_prevTotalUnits"];

//diag_log format ["## DOD2 ## 1 - fnc_makeGroupsFromNumber.sqf - _this: %1", _this];

_n = _this select 0;
_totalUnits = _this select 1;
//diag_log format ["## DOD2 ## 2 - fnc_makeGroupsFromNumber.sqf - _totalUnits: %1", _totalUnits];
_side = _this select 2;
_posArray = _this select 3;
_spawnPos = [(_posArray select 0), (_posArray select 1), (random 359)] call BIS_fnc_relPos;
_prevTotalUnits = if(count _this > 5) then { _this select 5; } else { 0; };
_grp = createGroup _side;
_DOD_grpArray = []; 
_DOD_grpArrayInput = [];

//diag_log format ["## DOD2 ## 3 - fnc_makeGroupsFromNumber.sqf - _DOD_grpArray: %1, _DOD_grpArrayInput: %2", _DOD_grpArray, _DOD_grpArrayInput];


if((count _this) > 4) then {
	_DOD_grpArrayInput = _this select 4;
};

//diag_log format ["## DOD2 ## 4 - fnc_makeGroupsFromNumber.sqf - _DOD_grpArray: %1, _DOD_grpArrayInput: %2", _DOD_grpArray, _DOD_grpArrayInput];

//diag_log format ["## 5 - count _this: %1", count _this];

// if _totalUnits - _n is bigger then 1, append array
if(((count _this) > 4) && ((_totalUnits - _n) >= 1) || ((count _this) > 4) && ((_prevTotalUnits - _n >= 1))) then {
	_DOD_grpArray = + _DOD_grpArrayInput;
};


if(_totalUnits < _n) then {
	_n = _totalUnits;
};


// check for surface is water
// if so, spawn divers and boats!
if(surfaceIsWater _spawnPos) then { // on water
	// create group 
	for "_i" from 0 to _n-1 do {
		// createUnits
		_unit = DOD_cnf_ai_divers call BIS_fnc_selectRandom;
		_unit createUnit [_spawnPos, _grp];
	};
} else { // not on water
	// create group 
	for "_i" from 0 to _n-1 do {
		// createUnits
		_unit = DOD_cnf_ai_units call BIS_fnc_selectRandom;
		_unit createUnit [_spawnPos, _grp];
	};
};

//diag_log format ["## DOD2 ## 6 - fnc_makeGroupsFromNumber.sqf - _grp: %1", _grp];

// adding group to array so it can be used later
_DOD_grpArray set [(count _DOD_grpArray), _grp];

// debug marker
private ["_mrkName", "_dbgMrk"];
_mrkName = "mrk_AI_"+str(round(random 10000)); // +str(_i)
_dbgMrk = createMarker [_mrkName, _spawnPos]; // position _grpLead
_dbgMrk setMarkerShape "ICON";
_dbgMrk setMarkerType "mil_dot";
_dbgMrk setMarkerColor "ColorRed";
_dbgMrk setMarkerText _mrkName;	


// set unit behaviour and skill
[_grp] call DOD_fnc_setUnitProperties;


// this will add the eventHandlers to each unit
[_grp] call DOD_fnc_addUnitEventhandlers;


// check how many units left to group
_rest = _totalUnits - _n;

//diag_log format ["## DOD2 ## 7 - fnc_makeGroupsFromNumber.sqf - _DOD_grpArray: %1", _DOD_grpArray];

// if more then 0, run this function again till no more groups can be made
if(_rest > 0) then {
	[_n, _rest, EAST, [(_posArray select 0), (_posArray select 1)], _DOD_grpArray, _totalUnits] call DOD_fnc_makeGroupsFromNumber;
} else {
	// return the array
	_DOD_grpArray
};


// EOF