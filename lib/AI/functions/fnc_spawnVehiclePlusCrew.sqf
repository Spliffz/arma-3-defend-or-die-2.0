//  Defend Or Die 2.0
//  fnc_spawnVehiclePlusCrew.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

// keep them local vars private
private ["_type", "_spawnPos", "_side", "_vicGrp", "_veh"];

_type = _this select 0;
_spawnPos = _this select 1;
_side = _this select 2;


// create vehicle
_vicGrp = createGroup _side;
_veh = _type createVehicle _spawnPos;
//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];


// fill it with crew
createVehicleCrew _veh;
_crew = crew _veh;
_crew joinsilent _vicGrp;
_vicGrp addVehicle _veh;


// return value
[_veh, _crew, _vicGrp]


// EOF