//  Defend Or Die 2.0
//  fnc_taskAttack.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>


// run on everything except a client box.
if!(isServer) exitWith {};

// keep them local variables private!
private ["_grp", "_pos", "_wp"];

// get data from params
_grp = _this select 0;
_pos = _this select 1;

// set waypoint 
_wp = _grp addWaypoint [_pos, 0];
_wp setWaypointType "SAD";
_wp setWaypointCompletionRadius 20;
_wp setWaypointCombatMode "RED";
_wp setWaypointSpeed "FULL";

// set group behavious
_grp setBehaviour "AWARE";


// return true to end script
true


// EOF