// Defend Or Die 2.0
// fnc_addUnitEventhandlers.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

// keep them local variables private!
private ["_grp", "_grpx"];

// get data from params
_grp = _this select 0;
_grpx = [];

// make an array from the params, if it isn't one
if(typeName _grp == "ARRAY") then {
    _grpx = _this select 0;
} else {
    _grpx = [];
    {
        _grpx set [(count _grpx), _x];
    } foreach units _grp;
};

// set _this to nothing, to ensure it can be re-used in the code below.
_this = nil;


// loop throught the group and add eventhandlers to each unit
{
	_x addEventHandler ["HitPart", {
		//diag_log format ["## DOD2 ## - _this select 0: %1" ,_this select 0];
		_this = _this select 0;
		//params: _target(obj), _shooter(obj), _bullet(obj), _position(Position3D ASL), _velocity(vector3D), _selection(array), _ammo(array), _direction(vector3D), _radius(int), _surface(str), _direct(bool)
		// headshot!
		//diag_log format ["## DOD2 ## - _this select 5: %1" ,_this select 5];
		if((_this select 5 select 0) == "head") then {
			DOD_pvc_n_enemyKilled_headshot = [format ["%1", name (_this select 0)], 50];
			(owner (_this select 1)) publicVariableClient "DOD_pvc_n_enemyKilled_headshot";
			(_this select 0) setDamage 1;
		};
	} ];
	
	_x addEventHandler ["Killed", {
		DOD_pvc_n_enemyKilled = [format ["%1", name (_this select 0)], 100];
		(owner (_this select 1)) publicVariableClient "DOD_pvc_n_enemyKilled";
	} ];
	
} foreach _grpx;

// set _grpx to nothing
_grpx = nil;

// return value to exit script
true


// EOF