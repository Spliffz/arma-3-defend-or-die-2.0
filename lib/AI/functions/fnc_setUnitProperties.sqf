// Defend Or Die 2.0
// fnc_setUnitProperties.sqf
// 2014 - Spliffz <theSpliffz@gmail.com


// keep them local variables private!
private ["_grp", "_grpx"];

// get data from params
_grp = _this select 0;
_grpx = [];

// make an array from the params, if it isn't one
if(typeName _grp == "ARRAY") then {
    _grpx = _this select 0;
} else {
    _grpx = [];
    {
        _grpx set [(count _grpx), _x];
    } foreach units _grp;
};

// set _this to nothing, to ensure it can be re-used.
_this = nil;


// loop through the group/array and set the properties for each unit
{
    _x allowFleeing 0;
} foreach _grpx;


// set skills (array format)
[_grpx] call DOD_fnc_setUnitSkill;


// set _grpx to nothing
_grpx = nil;

// return value to exit script
true


// EOF