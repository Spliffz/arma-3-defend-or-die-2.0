// Defend Or Die 2.0
// fnc_spawnVehicles.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

//[1, 2, 1, side, [xy, 100]] call DOD_fnc_spawnVehicles;

if((count _this) < 1) exitWith { diag_log format ["fnc_spawnVehicles needs atleast 1 parameter"]};

private ["_cars", "_armor", "_air", "_side", "_posArray", "_DOD_vicGrpArray", "_type_truck_transport", "_type_helo_attack", "_type_helo_transport"];

_cars = _this select 0;
_armor = _this select 1;
_air = _this select 2;
_side = _this select 3;
_posArray = _this select 4;
_DOD_vicGrpArray = [];
_type_truck_transport = ["O_Truck_02_covered_F","O_Truck_02_transport_F", "O_Truck_03_transport_F", "O_Truck_03_covered_F"];
_type_helo_attack = ["O_Heli_Attack_02_F","O_Heli_Attack_02_black_F"];
_type_helo_transport = ["O_Heli_Light_02_unarmed_F","O_Heli_Light_02_F"];

// spawn cars
if(_cars > 0) then {
	for "_i" from 0 to _cars-1 do {
		private ["_type", "_spawnPos", "_ret", "_grp", "_vehObj"];
		_spawnPos = [(_posArray select 0), (_posArray select 1), (random 359)] call BIS_fnc_relPos;
		_type = "";
		_ret = "";
		
		// check for surface is water
		// if so, spawn divers and boats!
		if(surfaceIsWater _spawnPos) then { // on water
			_type = DOD_cnf_ai_ships call BIS_fnc_selectRandom;
            /*
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            // fill it with crew
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
            
		} else {
			_type = DOD_cnf_ai_cars call BIS_fnc_selectRandom;
			/*
            //_ret = [_spawnPos, (random 359), _type, _side] call BIS_fnc_spawnVehicle; // did this first, but couldn't get the object of the spawned vehicle without getting the units the leave/re-enter it first.
            
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            _ret = [_veh, _vicGrp, _spawnPos];

            // fill it with crew
            //_crew = [_type, _vicGrp] call BIS_fnc_spawnCrew;
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
            
            /* WIP */
            // if _type == truck, fill it up with units
            if(_type in _type_truck_transport) then {
                //[_ret] call DOD_fnc_spawnTruck; // disabled, moveInCargo seems broken
            };

        };

		
		
		// debug marker
		private ["_mrkName", "_dbgMrk"];
		_mrkName = "mrk_AI_"+str(round(random 10000)); // +str(_i)
		_dbgMrk = createMarker [_mrkName, _spawnPos]; // position _grpLead
		_dbgMrk setMarkerShape "ICON";
		_dbgMrk setMarkerType "mil_dot";
		_dbgMrk setMarkerColor "ColorBlue";
		_dbgMrk setMarkerText _mrkName;	
		
		
		// walk through the crew, prep them
		_grp = _ret select 1;
		[_grp] call DOD_fnc_addUnitEventhandlers;
		
		// prep the vic
		_vehObj = _ret select 0;
		_vehObj addEventHandler ["Killed", {
			DOD_pvc_n_enemyCarKilled = [format ["%1", name (_this select 0)], 100];
			(owner (_this select 1)) publicVariableClient "DOD_pvc_n_enemyCarKilled";
		} ];
		
		
		// adding group to array so it can be used later
		_DOD_vicGrpArray set [(count _DOD_vicGrpArray), (_ret select 2)];

	};
};



// spawn armor
if(_armor > 0) then {
	for "_i" from 0 to _armor-1 do {
		private ["_type", "_spawnPos", "_ret", "_grp", "_vehObj"];
		_spawnPos = [(_posArray select 0), (_posArray select 1), (random 359)] call BIS_fnc_relPos;
		_type = "";
		_ret = "";
		
		// check for surface is water
		// if so, spawn divers and boats!
		if(surfaceIsWater _spawnPos) then { // on water
			_type = DOD_cnf_ai_ships call BIS_fnc_selectRandom;
            /*
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            // fill it with crew
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];
		} else {
			_type = DOD_cnf_ai_armor call BIS_fnc_selectRandom;
            /*
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            _ret = [_veh, _vicGrp, _spawnPos];

            // fill it with crew
            //_crew = [_type, _vicGrp] call BIS_fnc_spawnCrew;
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];

        };
		
		// debug marker
		private ["_mrkName", "_dbgMrk"];
		_mrkName = "mrk_AI_"+str(round(random 10000)); // +str(_i)
		_dbgMrk = createMarker [_mrkName, _spawnPos]; // position _grpLead
		_dbgMrk setMarkerShape "ICON";
		_dbgMrk setMarkerType "mil_dot";
		_dbgMrk setMarkerColor "ColorYellow";
		_dbgMrk setMarkerText _mrkName;	
		
		
		// walk through the crew, prep them
		_grp = _ret select 1;
		[_grp] call DOD_fnc_addUnitEventhandlers;


		// prep the vic
		_vehObj = _ret select 0;
		_vehObj addEventHandler ["Killed", {
			DOD_pvc_n_enemyCarKilled = [format ["%1", name (_this select 0)], 100];
			(owner (_this select 1)) publicVariableClient "DOD_pvc_n_enemyCarKilled";
		} ];
		
		
		// adding group to array so it can be used later
		_DOD_vicGrpArray set [(count _DOD_vicGrpArray), (_ret select 2)];

	};
};



// spawn air
if(_air > 0) then {
	for "_i" from 0 to _air-1 do {
		private ["_type", "_spawnPos", "_ret", "_grp", "_vehObj"];
		_spawnPos = [(_posArray select 0), (_posArray select 1)*2, (random 359)] call BIS_fnc_relPos;
		_type = "";
		_ret = "";
		
		// check for surface is water
		// if so, spawn divers and boats!
		if(surfaceIsWater _spawnPos) then { // on water
			_type = DOD_cnf_ai_ships call BIS_fnc_selectRandom;
            /*
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            // fill it with crew
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];
		} else {
			_type = DOD_cnf_ai_air call BIS_fnc_selectRandom;
            /*
            // create vehicle
            _vicGrp = createGroup _side;
            _veh = _type createVehicle _spawnPos;
			diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _veh: %1", _veh];

            _ret = [_veh, _vicGrp, _spawnPos];

            // fill it with crew
            //_crew = [_type, _vicGrp] call BIS_fnc_spawnCrew;
            createVehicleCrew _veh;
            _crew = crew _veh;
            _crew joinsilent _vicGrp;
            _vicGrp addVehicle _veh;
			
            //diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _crew: %1", _crew];
            
            _ret = [_veh, _crew, _vicGrp];
            */
            _ret = [_type, _spawnPos, _side] call DOD_fnc_spawnVehiclePlusCrew;
			//diag_log format ["## DOD2 ## - fnc_spawnVehicles.sqf - _ret: %1", _ret];
		};
		
		
		// debug marker
		private ["_mrkName", "_dbgMrk"];
		_mrkName = "mrk_AI_"+str(round(random 10000)); // +str(_i)
		_dbgMrk = createMarker [_mrkName, _spawnPos]; // position _grpLead
		_dbgMrk setMarkerShape "ICON";
		_dbgMrk setMarkerType "mil_dot";
		_dbgMrk setMarkerColor "ColorGreen";
		_dbgMrk setMarkerText _mrkName;	
		
		
		// walk through the crew, prep them
		_grp = _ret select 1;
		[_grp] call DOD_fnc_addUnitEventhandlers;


		// prep the vic
		_vehObj = _ret select 0;
		_vehObj addEventHandler ["Killed", {
			DOD_pvc_n_enemyCarKilled = [format ["%1", name (_this select 0)], 100];
			(owner (_this select 1)) publicVariableClient "DOD_pvc_n_enemyCarKilled";
		} ];
		
		
		// adding group to array so it can be used later
		_DOD_vicGrpArray set [(count _DOD_vicGrpArray), (_ret select 2)];

	};
};


// return
_DOD_vicGrpArray


// EOF