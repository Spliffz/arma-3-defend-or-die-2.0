// Defend Or Die 2.0
// fnc_spawnTruck.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// only run on server
//if(!isServer) exitWith {};

// Keep them local variables private!
private ["_truckParams","_truckObj","_truckCrew","_truckGrp","_spawnPos"];

diag_log format ["## DOD2 ## - fnc_spawnTruck.sqf - _this: %1", _this];
_truckParams = _this select 0;
diag_log format ["## DOD2 ## - fnc_spawnTruck.sqf - _truckParams: %1", _truckParams];
_truckObj = _truckParams select 0;
_truckGrp = _truckParams select 1;
_spawnPos = _truckParams select 2;

// truck holds 15 places, excluding driver
for "_i" from 0 to 14 do {
    private ["_unit"];
	// spawn unit
	_unit = DOD_cnf_ai_units call BIS_fnc_selectRandom;
    _unit createUnit [_spawnPos, _truckGrp];
    
	// place in truck
    diag_log format ["## DOD2 ## - fnc_spawnTruck.sqf - _truckObj: %1", _truckObj];
    //_unit assignAsCargo vehicle _truckObj;
	_unit moveInCargo [_truckObj, 1];  // Error moveincargo: Type String, expected Object - W.T.F. It is an object!
};


// return true to end script
true


// EOF