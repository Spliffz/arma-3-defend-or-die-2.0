// Defend Or Die 2.0
// ai_init.sqf
// 2014 - Spliffz <theSpliffz@gmail.com

diag_log format ["## DOD2 ## - ai_init.sqf - Starting..."];

// keep them local variables private!
private ["_c","_multiplier", "_unitCount", "_maxSpawnDistance", "_defPos", "_eastHQ", "_endtrg"];

// wait for notification to fade
sleep (3+(random 3));

// important vars - DON'T CHANGE!!!
DOD_round = 1;
_multiplier = 1.5;
_defPos = getMarkerPos "DOD_mrk_defPos";
_maxSpawnDistance = 400+(random 100);
_eastHQ = createCenter DOD_cnf_enemy_side;

/*
// create end condition trigger
_endtrg = createTrigger ["EmptyDetector", _defPos];
_endtrg setTriggerArea [(_maxSpawnDistance*2), (_maxSpawnDistance*2), true];
_endtrg setTriggerActivation [format ["%1",DOD_cnf_enemy_side], "NOT PRESENT", true]; 
_endtrg setTriggerType "END1";
*/

// the magic for loop that handles the enemies
for "_i" from 0 to DOD_round_max-1 do {
	private ["_grps","_spawnPos", "_sleep", "_vicGrps"];

   	diag_log format ["## DOD2 ## - ai_init.sqf - DOD_round: %1", DOD_round];

	// Show next round notification
	DOD_n_startNextRound = [DOD_round];
	[DOD_n_startNextRound, "DOD_fnc_n_startNextRound", true, true] call BIS_fnc_MP;

	// AI multiplier, so that you don't get hordes of enemies when you're playing alone
	_c = {(isPlayer _x) && (alive _x)} count allUnits;
	_unitCount = round((_c*_multiplier)*DOD_round);
	diag_log format ["## DOD2 ## - ai_init.sqf - _unitCount: %1", _unitCount];
	
	// spawn AI group around player
	_spawnPos = [_defPos, _maxSpawnDistance, (random 359)] call BIS_fnc_relPos;
	diag_log format ["## DOD2 ## - ai_init.sqf - _spawnPos: %1", _spawnPos];
	
	// spawn the group(s)
	_grps = [9, _unitCount, DOD_cnf_enemy_side, [_defPos, _maxSpawnDistance]] call DOD_fnc_makeGroupsFromNumber;
	
	// loop through the just made array and give all the groups something to do
	{
		// make them attack player
		//[_x, _defPos] call BIS_fnc_taskAttack;
		[_x, _defPos] call DOD_fnc_taskAttack;
	} foreach _grps;
	
	
	// vehicles!
	if(DOD_round >= 3 && DOD_round < 7) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [1,0,0,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	if(DOD_round >= 7 && DOD_round < 10) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [2,0,0,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};

	if(DOD_round >= 10 && DOD_round < 13) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [2,1,0,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	if(DOD_round >= 13 && DOD_round < 16) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [2,2,0,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	if(DOD_round >= 16 && DOD_round < 19) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [3,3,0,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	if(DOD_round >= 19 && DOD_round < 22) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [3,3,1,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	// Final rounds
	if(DOD_round >= 22) then {
//		if(random 1 > 0.3) then {
			_vicGrps = [5,5,3,DOD_cnf_enemy_side,[_defPos, _maxSpawnDistance]] call DOD_fnc_spawnVehicles;
			{
				// make them attack player
				//[_x, _defPos] call BIS_fnc_taskAttack;
                [_x, _defPos] call DOD_fnc_taskAttack;
			} foreach _vicGrps;
//		};
	};
	
	// prep timer
	if(_c > 1) then {
		_sleep = (DOD_round*100)/_c+(_maxSpawnDistance/8);
	} else {
		//_sleep = (DOD_round*100)/3.33; //+(_maxSpawnDistance/4); 	// +50s each round
		_sleep = (DOD_round*50)/2+(_maxSpawnDistance/16); 	// +50s each round
	};
    
	diag_log format ["## DOD2 ## - ai_init.sqf: _sleep: %1", _sleep];
	
	// set round +1
	if(DOD_round != DOD_round_max) then {
		DOD_round = DOD_round+1;
	} else {
		// continue
	};
	
    
    // update skills array with new values for next round
    call DOD_fnc_updateSkillArray;
    
    
	// sleep till next wave
	sleep _sleep;
	
};


// EOF