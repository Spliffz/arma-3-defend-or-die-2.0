//  Defend Or Die 2.0
//  starthcam.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>


// if helmetcam is enabled in parameters, start it
if(DOD_p_misc_helmetCam == 1) then {
   	nul = ["mission"] execVM "lib\hcam\hcam_init.sqf";
};


// EOF