// Defend Or Die 2.0
// PublicVariableServer Handler
// pvs_handler.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

// don't run this on clients
if(!isServer) exitWith {};


// setup PublicVariableEventHandlers - the magic start thingie
"DOD_pv_startup" addPublicVariableEventHandler {
	[_this] call DOD_startup;
};

// createObject eventhandler - this creates an client-side chosen object serverside
"DOD_pv_createObject" addPublicVariableEventHandler {
	private ["_params", "_obj", "_fort"];
	
	// get data from publicvariable
	_params = _this select 0;
	_obj = _this select 1;

	// spawn the object
	_fort = _obj createVehicle (getPosATL DOD_obj_spawnpoint);
	
	// run it through Player-Object-Mover to make it movable by player
	[_fort, "DOD_POM_init", true, true] call BIS_fnc_MP;
};


// client ID request EH
"DOD_pvs_requestClientID" addPublicVariableEventHandler {
	private ["_params", "_obj"];
	
	// get data from publicvariable
	_params = _this select 0;
	_obj = _this select 1;

    // set clientID as player variable so that we can use it later as clients
    _obj setVariable ["DOD_clientID", owner _obj, true];
}; 
  

// EOF