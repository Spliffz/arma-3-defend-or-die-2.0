//  Defend Or Die 2.0
//  init_server.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>

// if not server, don't run this script
if(!isServer) exitWith {};


// include the PublicVariable Eventhandlers
#include "pvs_handler.sqf"

// set time and date
call DOD_fnc_setTimeDate;

// set weather



// return true, to end script
true


// EOF