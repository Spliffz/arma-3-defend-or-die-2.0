//  Defend Or Die 2.0
//  init_hud.sqf
//  2014 - Spliffz <theSpliffz@gmail.com>


private ["_ui", "_hud"];

disableSerialization;

48015 cutRsc ["dod_gui", "PLAIN", 1, false];

// hide Round counter
_ui = uiNamespace getVariable ["dod_gui_hud", false];
_hud = _ui displayCtrl 1112;
_hud ctrlSetFade 1;
_hud ctrlCommit 0;

// hide total kills counter
_hud = _ui displayCtrl 1111;
_hud ctrlSetFade 1;
_hud ctrlCommit 0;

// set correct values & alignment for player kill counter
_hud = _ui displayCtrl 1110;
_hud ctrlSetFade 0;
_hud ctrlSetStructuredText parseText format ["<t align='right'>Your kills: 0</t>"];
_hud ctrlCommit 0;

// set correct values & alignment for player points counter
_hud = _ui displayCtrl 1113;
_hud ctrlSetFade 0;
_hud ctrlSetStructuredText parseText format ["<t align='right'>Your Points: 0</t>"];
_hud ctrlCommit 0;


// EOF