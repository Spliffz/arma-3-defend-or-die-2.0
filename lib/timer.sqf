// Defend Or Die 2.0
// timer.sqf
// 2014 - Spliffz <theSpliffz@gmail.com
// source: bohemia interactive forums somewhere, all credit goes to the original author.


// Start the timer serverside
if(isServer) then {
	DOD_END_TIME = 300;
    /* enable when final
    _c = {(isPlayer _x) && (alive _x)} count allUnits;
    DOD_END_TIME = 60*_c;
	*/
    publicvariable "DOD_END_TIME";
};

waituntil {!isNil "DOD_END_TIME"};

if(isServer) then {
	[] spawn {
		DOD_ELAPSED_TIME = 0;
		DOD_START_TIME = diag_tickTime;
		while {(DOD_ELAPSED_TIME < DOD_END_TIME)} do 
		{
			DOD_ELAPSED_TIME = diag_tickTime - DOD_START_TIME;
			publicVariable "DOD_ELAPSED_TIME";
			sleep 1;
		};
		// Party-ehhh!
		if ((DOD_ELAPSED_TIME > DOD_END_TIME)) then {
			// show notification
			DOD_n_startAttack = 1;
			[DOD_n_startAttack, "DOD_n_startAttackNotification"] call BIS_fnc_MP;
			
			// start AI script
			[] spawn DOD_AI_init;
		};
	};
};


if!(isDedicated) then {
	[] spawn {	
		sleep 1;
		waituntil {!isNil "DOD_ELAPSED_TIME"};
		while{DOD_ELAPSED_TIME < DOD_END_TIME } do
		{
			_time = DOD_END_TIME - DOD_ELAPSED_TIME;
			_finish_time_minutes = floor(_time / 60);
			_finish_time_seconds = floor(_time) - (60 * _finish_time_minutes);
			if(_finish_time_seconds < 10) then {
				_finish_time_seconds = format ["0%1", _finish_time_seconds];
			};
			if(_finish_time_minutes < 10) then {
				_finish_time_minutes = format ["0%1", _finish_time_minutes];
			};
			
			DOD_formatted_time = format ["%1:%2", _finish_time_minutes, _finish_time_seconds];
			
			hintSilent format ["Time left:\n%1", DOD_formatted_time];
			sleep 1;
		};
	};
};


// EOF